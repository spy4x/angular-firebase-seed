module.exports = {
  projects: [
    '<rootDir>/apps/front/web-client',
    '<rootDir>/apps/front/admin-panel',
    '<rootDir>/apps/back/cloud-functions',
    '<rootDir>/apps/dev/sandbox',
    '<rootDir>/apps/dev/deploy',
    '<rootDir>/libs/front/web-client/core',
    '<rootDir>/libs/front/admin-panel/core',
    '<rootDir>/libs/back/api/core',
    '<rootDir>/libs/back/api/users',
    '<rootDir>/libs/back/api/shared',
    '<rootDir>/libs/dev/utils',
  ],
};

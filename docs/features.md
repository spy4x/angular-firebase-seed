# List of features

* Realtime lists of todos - like Google Keep
* Group access - invite your friends and cowork on your lists together
* Share a list of todos to a person outside of your group or make it public
* Transfer ownership of a personal list to a group and visa versa
* Upload a txt/csv file with a list of todos
* Manage your personal account - avatar, email, oAuth providers, groups memberships
* Manage group account - avatar, members, accesses

# Angular Firebase Seed


## Purpose

After working on several Angular + Firebase projects I've got some experience of how to properly structure such a project. All pain, wasted hours and broken keyboards as well as best practices and reusable components and modules are concentrated in this repo.

Idea is to clone this project when we need to work on our next project/hackathon/pet project/anything. This repo contains minimum of features, but most of reusable stuff from project to project.


## Implementation

This app is a ToDo list app on muscles.

[Check out list of features here](docs/features.md)


## How-to

Looking for answers of your technical questions? [Check it out here](docs/how-to.md)


## Development commands

This project was generated using [Nx](https://nx.dev).

[Check out instruction for developers here](docs/nx.md)


[Custom commands and info](docs/custom.md)

